
USE `world`;

select * from country where Continent LIKE "Europe";

-- Largest countries
SELECT MAX(SurfaceArea), Name from country GROUP BY Name ORDER BY MAX(SurfaceArea) DESC LIMIT 20;

-- Countries with highest GNP
SELECT MAX(GNP), Name from country GROUP BY Name ORDER BY MAX(GNP) DESC LIMIT 20;

-- Type of goverment forms 
select GovernmentForm, count(GovernmentForm) AS NbCountries from country 
group by GovernmentForm ORDER BY count(GovernmentForm) DESC;


-- People speaking English
Select sum((country.Population*countrylanguage.Percentage)/100) FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
WHERE Language LIKE 'English';

SELECT cl.CountryCode, co.Name, cl.Percentage
FROM countrylanguage cl
JOIN country co ON co.Code = cl.CountryCode
WHERE cl.IsOfficial = 'T' AND cl.Language LIKE 'English';

-- Liste of languages from more spoken to less
SELECT countrylanguage.Language, (country.Population)/100) AS TotalPopulation from countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY countrylanguage.Language ORDER BY TotalPopulation DESC;

-- Countries with more than 10 000 000 its capital and the % of people living in the capital
Select country.Name, city.Name, country.Population,((city.Population*100)/country.Population) AS PercentageCapital FROM country
JOIN city ON country.Capital = city.ID
WHERE country.Population > 10000000 ORDER BY country.Population DESC;

-- 10 Countries with highest growing rate (GNP-GNPOld*100)/GNPOld
SELECT Name, ((GNP - GNPOld) / GNPOld) * 100 AS GrowthRate FROM country
ORDER BY GrowthRate DESC LIMIT 10;

-- 8_Countries with more than 1 language

SELECT country.Name, COUNT(countrylanguage.CountryCode) AS NbLanguages FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY country.Name
HAVING COUNT(countrylanguage.CountryCode) > 1
ORDER BY NbLanguages DESC;

--9_Liste des pays avec plusieurs langues officielles, 
--le nombre de langues officielle et le nombre de langues du pays

SELECT country.Name, COUNT(countrylanguage.IsOfficial) AS "Nb of Official Languages"
FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
WHERE countrylanguage.IsOfficial = 'T'
GROUP BY country.Name
HAVING COUNT(countrylanguage.IsOfficial) > 1
ORDER BY COUNT(countrylanguage.IsOfficial) DESC;

--the condition: COUNT(countrylanguage.IsOfficial) > 1, cant be used in the WHERE

SELECT 
    country.Name,
    COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) AS "Nb of Official Languages",
    COUNT(countrylanguage.Language) AS "Total Nb of Languages"
FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY country.Name
HAVING COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) > 1
ORDER BY COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) DESC;


-- 10 Languages spoken in France, China, etc...
SELECT country.Name, countrylanguage.language, countrylanguage.Percentage from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE "France";


-- 15 addition of percentages

SELECT country.Name, SUM(countrylanguage.Percentage) AS TotalPercentage FROM country
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code
GROUP BY countrylanguage.CountryCode ORDER BY TotalPercentage DESC;


SELECT country.Name, countrylanguage.language, countrylanguage.Percentage, countrylanguage.IsOfficial from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE 'Netherlands';

/*-----------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
---------------------------------PART 2---------------------------------------------------*/

--MIGRATION ADD LANGUAGE TO CUSTOMERS TABLE. 1st GET A PROJECTION FROM world with the country and most spoken language
SELECT cl.countryCode, cl.language, cl.Percentage FROM countrylanguage cl
JOIN (SELECT countryCode,MAX(Percentage) AS maxPercentage FROM countrylanguage
    GROUP BY countryCode) max_lang
ON cl.countryCode = max_lang.countryCode AND cl.Percentage = max_lang.maxPercentage;

--Option 2 CP
SELECT countryCode, language, Percentage
FROM (
    SELECT 
        countryCode, 
        language, 
        Percentage,
        RANK() OVER (PARTITION BY countryCode ORDER BY Percentage DESC) AS language_rank
    FROM countrylanguage
) ranked_languages
WHERE language_rank = 1;

--1_Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays 
    --(si plusieurs, peu importe laquelle), le CA total

SELECT c.country,COUNT(DISTINCT c.customerName) AS numberOfClients, SUM(p.amount) AS totalRevenue FROM customers c
JOIN payments p ON c.customerNumber = p.customerNumber GROUP BY c.country;


--2_La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant
SELECT customers.customerName, SUM(orderdetails.quantityOrdered * orderdetails.priceEach) AS TotalRevenue FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
GROUP BY customers.customerName ORDER BY TotalRevenue DESC LIMIT 10;

--3_La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande)
SELECT AVG(DATEDIFF(shippedDate, orderDate)) AS total_average_days
FROM orders;

--4_Les 10 produits les plus vendus
SELECT products.productName, SUM(orderdetails.quantityOrdered) AS TotalQuantity FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY TotalQuantity DESC limit 10;

--5_Pour chaque pays le produits le plus vendu.
SELECT products.productName, SUM(orderdetails.quantityOrdered) AS TotalQuantity FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY TotalQuantity DESC limit 10;


--6_Le produit qui a rapporté le plus de bénéfice.
SELECT products.productName, SUM(orderdetails.quantityOrdered*orderdetails.priceEach) AS MaxRevenu FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY MaxRevenu DESC limit 1;

SELECT products.productName, MAX(SUM(orderdetails.quantityOrdered*orderdetails.priceEach)) AS MaxRevenu FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY MaxRevenu DESC limit 1;

--7_La moyenne des différences entre le MSRP et le prix de vente (en pourcentage)

SELECT products.productName, 
AVG(((products.MSRP - orderdetails.priceEach) / products.MSRP) * 100) AS average_percentage FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode 
GROUP by products.productName ORDER BY products.productName LIMIT 20;




