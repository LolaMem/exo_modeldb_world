
Use classicmodels;

/* Insert the data from the original db classicmodels into the copy db classicmodels2
Starting with the tables without Foreign Keys or dependancies to other tables: 
productlines > products > offices > customers/empoyees > orders > oderdetails/payments*/

INSERT classicmodels2.productlines SELECT * FROM classicmodels.productlines;
INSERT classicmodels2.products SELECT * FROM classicmodels.products;

/* Update attribute country in tables customers and offices to show the field country.Code from DB world
 Normalizing USA and UK to the be comparable to the equivalent in world DB
 Verify which countries in customers and offices:
 select country, count(country) from customers group by country order by country;*/

UPDATE classicmodels2.offices SET country = "United Kingdom" WHERE country = "UK";
UPDATE classicmodels2.offices SET country = "United States" WHERE country = "USA";

INSERT INTO classicmodels2.offices (
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    country,
    postalCode,
    territory
)
SELECT
    officeCode,
    city,
    phone,
    addressLine1,
    addressLine2,
    state,
    world.country.code,
    postalCode,
    territory
FROM classicmodels.offices
JOIN world.country ON classicmodels.offices.country = world.country.Name;

SELECT city, country FROM classicmodels2.offices;

INSERT classicmodels2.employees SELECT * FROM classicmodels.employees;

/* Create a temporary table for customers, where modifying/normalize the country names 
between DB world & DB classicmodels for Norway, Russian Federation, United Kingdom, United States*/

create table tmp (
  customerNumber int,
  customerName varchar(50) NOT NULL,
  contactLastName varchar(50) NOT NULL,
  contactFirstName varchar(50) NOT NULL,
  phone varchar(50) NOT NULL,
  addressLine1 varchar(50) NOT NULL,
  addressLine2 varchar(50) DEFAULT NULL,
  city varchar(50) NOT NULL,
  state varchar(50) DEFAULT NULL,
  postalCode varchar(15) DEFAULT NULL,
  country varchar(50) NOT NULL,
  salesRepEmployeeNumber int DEFAULT NULL,
  creditLimit decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (customerNumber),
  FOREIGN KEY (salesRepEmployeeNumber) REFERENCES employees (employeeNumber)
);
insert into tmp table classicmodels.customers;
UPDATE tmp SET country = "United Kingdom" WHERE country = "UK";
UPDATE tmp SET country = "United States" WHERE country = "USA";
UPDATE tmp SET country = "Russian Federation" WHERE country = "Russia";
UPDATE tmp SET country = "Norway" WHERE country = "Norway  ";

INSERT INTO classicmodels2.customers (
    customerNumber,
    customerName,
    contactLastName,
    contactFirstName,
    phone,
    addressLine1,
    addressLine2,
    city,
    state,
    postalCode,
    country,
    salesRepEmployeeNumber,
    creditLimit
)
SELECT
    customerNumber,
    customerName,
    contactLastName,
    contactFirstName,
    phone,
    addressLine1,
    addressLine2,
    city,
    state,
    postalCode,
    world.country.code,
    salesRepEmployeeNumber,
    creditLimit
FROM tmp
JOIN world.country ON tmp.country = world.country.Name;

/*SELECT tmp.country AS country1,classicmodels2.customers.country AS country2,COUNT(classicmodels2.customers.country) AS countryCount
FROM classicmodels2.customers
JOIN tmp ON classicmodels2.customers.customerNumber = tmp.customerNumber
GROUP BY country1, country2 ORDER BY country1;*/

drop table tmp;

/* https://en.wikipedia.org/wiki/List_of_ISO_639_language_codes*/
ALTER TABLE classicmodels2.customers ADD COLUMN language char(2);


/* Change status to an enum:     select status from orders group by status;*/
ALTER TABLE classicmodels2.orders MODIFY COLUMN status enum('In Process','On Hold','Resolved', 'Shipped', 'Disputed', 'Cancelled');
/*ALTER TABLE classicmodels2.orders MODIFY COLUMN status enum(
SELECT status FROM classicmodels.orders GROUP BY status);*/
INSERT classicmodels2.orders SELECT * FROM classicmodels.orders;

SELECT status FROM classicmodels2.orders GROUP BY status;

INSERT classicmodels2.orderdetails SELECT * FROM classicmodels.orderdetails;
INSERT classicmodels2.payments SELECT * FROM classicmodels.payments;

