# DATABASE WORLD

Create a markdown file with :
* The physical model of the data
* For each type used for at least one attribute: define it and explain why this type was chosen ?
* An "explanation" or "description" of the information that can be extracted from each column in each table.
        a list, for each table, of the constraints (why are there these constraints?)
## General description of the DB
Data from 2002 et 2005.
There are 3 tables: city, country and countrylanguage, with 3998 entries in city, 239 countries and 458 languages

## Physical model of the data 

![Alt text](image.png)

## 2_Data Type:
* int => Integer type, used for ID, population, etc...
* smallint => Integer type, for small numbers, used for Year
* char(arg) => character type with fixed length, independently of the real length
* decimal(arg) => Used instead of a float, when precicion is crucial.Used for the country area, GNP, etc... 
* enum => Not a data type, a constraint in the data introduced that can only be from a list (Continents, or a boolean)

## 3_Explanation of attributes:

### In table city, the attributes are: ID, Name, CountryCode, District and Population
* ID is the primary key, it's unique and autoincremental
* Name of the city in its own language, with utf8mb4 code
* CountryCode, is an FK referring to Code in table country. It is the 3 letters international code for countries,  ISO 3166:  https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
* District is the region or state of the city within the country, i.e.: Chicago Illinois, London England.
* Population, total quantity of people registered in the city

### In table country, the attributes are: Code, Name, Continent, Region , SurfaceArea, IndepYear, Population, LifeExpectancy, GNP, GNPOld, LocalName, GovermentForm, HeadOfState, Capital and Code2
 * Code is the PK, and is referred as CountryCode (FK) in tables city and countrylanguage,
 * Name in English, in contrast to Localname, in the local language,
 * Continent is and enum data type, with a list of the continents,
 * Region is a generic geographical area within the continent: Italy in Southern Europe,
 * SurfaceArea is the area in km2,
 * IndepYear the year of the independence,
 * Population 
 * LifeExpectancy is the average life expectancy in years, decimal type,
 * GNP is the Gross National Product in $M,
 * GNP Old, could be the previous year's GNP,
 * GovermentForm the type of goverment; Federation, Republic, ...there are 35 different types,
 * HeadOfState The official sovereign representant depending on the goverment type, president, PM, monarch, etc,
 * Capital is a foreign key, refers to table city ID,
 * Code2 refers to a 2 letters international code for countries

### In table countrylanguage, the attributes are: CountryCode, Language, IsOfficial and Percentage
* CountryCode is an FK referring to Code in table country
* Language in English
* IsOfficial can be T or, True if is the official national language, 
* Percentage the ratio from 0 to 100 of people speaking the language


```sql    
SET @old_autocommit=@@autocommit;

CREATE DATABASE `world` DEFAULT CHARACTER SET utf8mb4;

USE `world`;


DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `city` (
  `ID` int NOT NULL AUTO_INCREMENT,
  `Name` char(35) NOT NULL DEFAULT '',
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `District` char(20) NOT NULL DEFAULT '',
  `Population` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `Code` char(3) NOT NULL DEFAULT '',
  `Name` char(52) NOT NULL DEFAULT '',
  `Continent` enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') NOT NULL DEFAULT 'Asia',
  `Region` char(26) NOT NULL DEFAULT '',
  `SurfaceArea` decimal(10,2) NOT NULL DEFAULT '0.00',
  `IndepYear` smallint DEFAULT NULL,
  `Population` int NOT NULL DEFAULT '0',
  `LifeExpectancy` decimal(3,1) DEFAULT NULL,
  `GNP` decimal(10,2) DEFAULT NULL,
  `GNPOld` decimal(10,2) DEFAULT NULL,
  `LocalName` char(45) NOT NULL DEFAULT '',
  `GovernmentForm` char(45) NOT NULL DEFAULT '',
  `HeadOfState` char(60) DEFAULT NULL,
  `Capital` int DEFAULT NULL,
  `Code2` char(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `countrylanguage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countrylanguage` (
  `CountryCode` char(3) NOT NULL DEFAULT '',
  `Language` char(30) NOT NULL DEFAULT '',
  `IsOfficial` enum('T','F') NOT NULL DEFAULT 'F',
  `Percentage` decimal(4,1) NOT NULL DEFAULT '0.0',
  PRIMARY KEY (`CountryCode`,`Language`),
  KEY `CountryCode` (`CountryCode`),
  CONSTRAINT `countryLanguage_ibfk_1` FOREIGN KEY (`CountryCode`) REFERENCES `country` (`Code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
```

## DDBB Queries
**1_Liste des type de gouvernement avec nombre de pays pour chaque.**
```sql
select GovernmentForm, count(GovernmentForm) AS NbCountries from country 
group by GovernmentForm ORDER BY count(GovernmentForm) DESC;
+----------------------------------------------+-------------+
| GovernmentForm                               | NbCountries |
+----------------------------------------------+-------------+
| Republic                                     |         123 |
| Constitutional Monarchy                      |          29 |
| Federal Republic                             |          14 |
| Dependent Territory of the UK                |          12 |
| Monarchy                                     |           5 |
| Overseas Department of France                |           4 |
| Nonmetropolitan Territory of France          |           4 |
| Constitutional Monarchy, Federation          |           4 |
| Territory of Australia                       |           4 |
| Socialistic Republic                         |           3 |
| Nonmetropolitan Territory of New Zealand     |           3 |
| US Territory                                 |           3 |
| Commonwealth of the US                       |           2 |
| Territorial Collectivity of France           |           2 |
| Nonmetropolitan Territory of The Netherlands |           2 |
| Dependent Territory of Norway                |           2 |
| Monarchy (Sultanate)                         |           2 |
| Part of Denmark                              |           2 |
| Special Administrative Region of China       |           2 |
| Islamic Republic                             |           2 |
| Federation                                   |           1 |
| Socialistic State                            |           1 |
| Autonomous Area                              |           1 |
| Administrated by the UN                      |           1 |
| Dependent Territory of the US                |           1 |
| Independent Church State                     |           1 |
| Parlementary Monarchy                        |           1 |
| Constitutional Monarchy (Emirate)            |           1 |
| Occupied by Marocco                          |           1 |
| People'sRepublic                             |           1 |
| Monarchy (Emirate)                           |           1 |
| Co-administrated                             |           1 |
| Emirate Federation                           |           1 |
| Parliamentary Coprincipality                 |           1 |
| Islamic Emirate                              |           1 |
+----------------------------------------------+-------------+
35 rows in set 
```
**2_Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?**
 Year of creation of the table 2005, conflict with datatype boolean for the current SQL version

**3_D’apres la BDD, combien de personne dans le monde parle anglais ?**
```sql
Select sum((country.Population*countrylanguage.Percentage)/100) AS TotalEnglishSpoken FROM country
JOIN countrylanguage ON country.Code = countrylanguage.CountryCode
WHERE Language LIKE 'English';

+--------------------+
| TotalEnglishSpoken |
+--------------------+
|    347077867.30000 |
+--------------------+
1 row in set (0.00 sec)
```

**4_Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.**
```sql
SELECT countrylanguage.Language, sum((country.Population*countrylanguage.Percentage)/100) AS TotalPopulation from countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY countrylanguage.Language ORDER BY TotalPopulation DESC LIMIT 20;

-- Output 458 rows, shown 20 first
+------------+------------------+
| Language   | TotalPopulation  |
+------------+------------------+
| Chinese    | 1191843539.00000 |
| Hindi      |  405633070.00000 |
| Spanish    |  355029462.00000 |
| English    |  347077867.30000 |
| Arabic     |  233839238.70000 |
| Bengali    |  209304719.00000 |
| Portuguese |  177595269.40000 |
| Russian    |  160807561.30000 |
| Japanese   |  126814108.00000 |
| Punjabi    |  104025371.00000 |
| German     |   92133584.70000 |
| Javanese   |   83570158.00000 |
| Telugu     |   79065636.00000 |
| Marathi    |   75019094.00000 |
| Korean     |   72291372.00000 |
| Vietnamese |   70616218.00000 |
| French     |   69980880.40000 |
| Tamil      |   68691536.00000 |
| Urdu       |   63589470.00000 |
| Turkish    |   62205657.20000 |
+------------+------------------+
20 rows in set (0.00 sec)
```

**5_En quelle unité est exprimée la surface des pays ?**
In Km2
**6_Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.**

```sql
Select country.Name, city.Name, country.Population,((city.Population*100)/country.Population) AS PercentageCapital FROM country
JOIN city ON country.Capital = city.ID
WHERE country.Population > 10000000 ORDER BY country.Population DESC;

+---------------------------------------+---------------------+------------+-------------------+
| Name                                  | Name                | Population | PercentageCapital |
+---------------------------------------+---------------------+------------+-------------------+
| China                                 | Peking              | 1277558000 |            0.5849 |
| India                                 | New Delhi           | 1013662000 |            0.0297 |
| United States                         | Washington          |  278357000 |            0.2055 |
| Indonesia                             | Jakarta             |  212107000 |            4.5283 |
| Brazil                                | Brasília            |  170115000 |            1.1580 |
| Pakistan                              | Islamabad           |  156483000 |            0.3352 |
| Russian Federation                    | Moscow              |  146934000 |            5.7095 |
| Bangladesh                            | Dhaka               |  129155000 |            2.7973 |
| Japan                                 | Tokyo               |  126714000 |            6.2978 |
| Nigeria                               | Abuja               |  111506000 |            0.3140 |
| Mexico                                | Ciudad de México    |   98881000 |            8.6885 |
| Germany                               | Berlin              |   82164700 |            4.1218 |
| Vietnam                               | Hanoi               |   79832000 |            1.7662 |
| Philippines                           | Manila              |   75967000 |            2.0813 |
| Egypt                                 | Cairo               |   68470000 |            9.9160 |
| Iran                                  | Teheran             |   67702000 |            9.9832 |
| Turkey                                | Ankara              |   66591000 |            4.5624 |
| Ethiopia                              | Addis Abeba         |   62565000 |            3.9879 |
| Thailand                              | Bangkok             |   61399000 |           10.2936 |
| United Kingdom                        | London              |   59623400 |           12.2184 |
| France                                | Paris               |   59225700 |            3.5884 |
| Italy                                 | Roma                |   57680000 |            4.5832 |
| Congo, The Democratic Republic of the | Kinshasa            |   51654000 |            9.8037 |
| Ukraine                               | Kyiv                |   50456000 |            5.2006 |
| South Korea                           | Seoul               |   46844000 |           21.3082 |
| Myanmar                               | Rangoon (Yangon)    |   45611000 |            7.3704 |
| Colombia                              | Santafé de Bogotá   |   42321000 |           14.7937 |
| South Africa                          | Pretoria            |   40377000 |            1.6312 |
| Spain                                 | Madrid              |   39441700 |            7.2995 |
| Poland                                | Warszawa            |   38653600 |            4.1791 |
| Argentina                             | Buenos Aires        |   37032000 |            8.0529 |
| Tanzania                              | Dodoma              |   33517000 |            0.5639 |
| Algeria                               | Alger               |   31471000 |            6.8889 |
| Canada                                | Ottawa              |   31147000 |            1.0764 |
| Kenya                                 | Nairobi             |   30080000 |            7.6130 |
| Sudan                                 | Khartum             |   29490000 |            3.2129 |
| Morocco                               | Rabat               |   28351000 |            2.1991 |
| Peru                                  | Lima                |   25662000 |           25.1917 |
| Uzbekistan                            | Toskent             |   24318000 |            8.7075 |
| Venezuela                             | Caracas             |   24170000 |            8.1725 |
| North Korea                           | Pyongyang           |   24039000 |           10.3332 |
| Nepal                                 | Kathmandu           |   23930000 |            2.4732 |
| Iraq                                  | Baghdad             |   23115000 |           18.7584 |
| Afghanistan                           | Kabul               |   22720000 |            7.8345 |
| Romania                               | Bucuresti           |   22455500 |            8.9783 |
| Taiwan                                | Taipei              |   22256000 |           11.8679 |
| Malaysia                              | Kuala Lumpur        |   22244000 |            5.8332 |
| Uganda                                | Kampala             |   21778000 |            4.0904 |
| Saudi Arabia                          | Riyadh              |   21607000 |           15.3839 |
| Ghana                                 | Accra               |   20212000 |            5.2939 |
| Mozambique                            | Maputo              |   19680000 |            5.1775 |
| Australia                             | Canberra            |   18886000 |            1.7088 |
| Sri Lanka                             | Colombo             |   18827000 |            3.4259 |
| Yemen                                 | Sanaa               |   18112000 |            2.7805 |
| Kazakstan                             | Astana              |   16223000 |            1.9183 |
| Syria                                 | Damascus            |   16125000 |            8.3535 |
| Madagascar                            | Antananarivo        |   15942000 |            4.2383 |
| Netherlands                           | Amsterdam           |   15864000 |            4.6092 |
| Chile                                 | Santiago de Chile   |   15211000 |           30.9247 |
| Cameroon                              | Yaoundé             |   15085000 |            9.1004 |
| Côte d’Ivoire                         | Yamoussoukro        |   14786000 |            0.8792 |
| Angola                                | Luanda              |   12878000 |           15.7012 |
| Ecuador                               | Quito               |   12646000 |           12.4423 |
| Burkina Faso                          | Ouagadougou         |   11937000 |            6.9029 |
| Zimbabwe                              | Harare              |   11669000 |           12.0833 |
| Guatemala                             | Ciudad de Guatemala |   11385000 |            7.2315 |
| Mali                                  | Bamako              |   11234000 |            7.2063 |
| Cuba                                  | La Habana           |   11201000 |           20.1411 |
| Cambodia                              | Phnom Penh          |   11168000 |            5.1053 |
| Malawi                                | Lilongwe            |   10925000 |            3.9905 |
| Niger                                 | Niamey              |   10730000 |            3.9143 |
| Yugoslavia                            | Beograd             |   10640000 |           11.3158 |
| Greece                                | Athenai             |   10545700 |            7.3212 |
| Czech Republic                        | Praha               |   10278100 |           11.4917 |
| Belgium                               | Bruxelles [Brussel] |   10239000 |            1.3073 |
| Belarus                               | Minsk               |   10236000 |           16.3540 |
| Somalia                               | Mogadishu           |   10097000 |            9.8742 |
| Hungary                               | Budapest            |   10043200 |           18.0376 |
+---------------------------------------+---------------------+------------+-------------------+
78 rows in set
```
**7_Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance**
```sql
SELECT Name, ((GNP - GNPOld) / GNPOld) * 100 AS GrowthRate FROM country
ORDER BY GrowthRate DESC LIMIT 10;

+---------------------------------------+------------+
| Name                                  | GrowthRate |
+---------------------------------------+------------+
| Congo, The Democratic Republic of the | 181.487470 |
| Turkmenistan                          | 119.850000 |
| Tajikistan                            |  88.446970 |
| Estonia                               |  58.053990 |
| Albania                               |  28.200000 |
| Suriname                              |  23.229462 |
| Iran                                  |  22.225899 |
| Bulgaria                              |  19.756122 |
| Honduras                              |  13.540558 |
| Latvia                                |  13.459833 |
+---------------------------------------+------------+
```
**8_Liste des pays plurilingues avec pour chacun le nombre de langues parlées.**
```sql
SELECT country.Name, COUNT(countrylanguage.CountryCode) AS NbLanguages FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY country.Name
HAVING COUNT(countrylanguage.CountryCode) > 1
ORDER BY NbLanguages DESC;

-- Output 212 with at least 2 languages, limited to 30
+---------------------------------------+-------------+
| Name                                  | NbLanguages |
+---------------------------------------+-------------+
| Canada                                |          12 |
| China                                 |          12 |
| India                                 |          12 |
| Russian Federation                    |          12 |
| United States                         |          12 |
| Tanzania                              |          11 |
| South Africa                          |          11 |
| Congo, The Democratic Republic of the |          10 |
| Iran                                  |          10 |
| Kenya                                 |          10 |
| Mozambique                            |          10 |
| Nigeria                               |          10 |
| Philippines                           |          10 |
| Sudan                                 |          10 |
| Uganda                                |          10 |
| Angola                                |           9 |
| Indonesia                             |           9 |
| Vietnam                               |           9 |
| Australia                             |           8 |
| Austria                               |           8 |
| Cameroon                              |           8 |
| Czech Republic                        |           8 |
| Italy                                 |           8 |
| Liberia                               |           8 |
| Myanmar                               |           8 |
| Namibia                               |           8 |
| Pakistan                              |           8 |
| Sierra Leone                          |           8 |
| Chad                                  |           8 |
| Togo                                  |           8 |
+---------------------------------------+-------------+
```
**9_Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.**

```sql
SELECT 
    country.Name,
    COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) AS "Nb of Official Languages",
    COUNT(countrylanguage.Language) AS "Total Nb of Languages"
FROM countrylanguage
JOIN country ON country.Code = countrylanguage.CountryCode
GROUP BY country.Name
HAVING COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) > 1
ORDER BY COUNT(CASE WHEN countrylanguage.IsOfficial = 'T' THEN 1 END) DESC;

+----------------------+--------------------------+-----------------------+
| Name                 | Nb of Official Languages | Total Nb of Languages |
+----------------------+--------------------------+-----------------------+
| South Africa         |                        4 |                    11 |
| Switzerland          |                        4 |                     4 |
| Luxembourg           |                        3 |                     5 |
| Vanuatu              |                        3 |                     3 |
| Singapore            |                        3 |                     3 |
| Peru                 |                        3 |                     3 |
| Bolivia              |                        3 |                     4 |
| Belgium              |                        3 |                     6 |
| Somalia              |                        2 |                     2 |
| Nauru                |                        2 |                     5 |
| Palau                |                        2 |                     4 |
| Paraguay             |                        2 |                     4 |
| Romania              |                        2 |                     6 |
| Rwanda               |                        2 |                     2 |
| Burundi              |                        2 |                     3 |
| Malta                |                        2 |                     2 |
| Seychelles           |                        2 |                     3 |
| Togo                 |                        2 |                     8 |
| Tonga                |                        2 |                     2 |
| Tuvalu               |                        2 |                     3 |
| American Samoa       |                        2 |                     3 |
| Samoa                |                        2 |                     3 |
| Netherlands Antilles |                        2 |                     3 |
| Marshall Islands     |                        2 |                     2 |
| Madagascar           |                        2 |                     2 |
| Afghanistan          |                        2 |                     5 |
| Lesotho              |                        2 |                     3 |
| Sri Lanka            |                        2 |                     3 |
| Kyrgyzstan           |                        2 |                     7 |
| Israel               |                        2 |                     3 |
| Ireland              |                        2 |                     2 |
| Guam                 |                        2 |                     5 |
| Greenland            |                        2 |                     2 |
| Faroe Islands        |                        2 |                     2 |
| Finland              |                        2 |                     5 |
| Cyprus               |                        2 |                     2 |
| Belarus              |                        2 |                     4 |
| Canada               |                        2 |                    12 |
+----------------------+--------------------------+-----------------------+
38 rows in set 
```
**10_Liste des langues parlées en France avec le % pour chacune.**

```sql
SELECT country.Name, countrylanguage.language,countrylanguage.Percentage from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE "France";

+--------+------------+------------+
| Name   | language   | Percentage |
+--------+------------+------------+
| France | Arabic     |        2.5 |
| France | French     |       93.6 |
| France | Italian    |        0.4 |
| France | Portuguese |        1.2 |
| France | Spanish    |        0.4 |
| France | Turkish    |        0.4 |
+--------+------------+------------+
```

**11_Pareil en chine.**
```sql
SELECT country.Name, countrylanguage.language, countrylanguage.Percentage from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE "China";

+-------+-----------+------------+
| Name  | language  | Percentage |
+-------+-----------+------------+
| China | Chinese   |       92.0 |
| China | Dong      |        0.2 |
| China | Hui       |        0.8 |
| China | Mantšu    |        0.9 |
| China | Miao      |        0.7 |
| China | Mongolian |        0.4 |
| China | Puyi      |        0.2 |
| China | Tibetan   |        0.4 |
| China | Tujia     |        0.5 |
| China | Uighur    |        0.6 |
| China | Yi        |        0.6 |
| China | Zhuang    |        1.4 |
+-------+-----------+------------+
12 rows in set 
```

**12_Pareil aux états unis.**
```sql
SELECT country.Name, countrylanguage.language, countrylanguage.Percentage from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE "United States";

+---------------+------------+------------+
| Name          | language   | Percentage |
+---------------+------------+------------+
| United States | Chinese    |        0.6 |
| United States | English    |       86.2 |
| United States | French     |        0.7 |
| United States | German     |        0.7 |
| United States | Italian    |        0.6 |
| United States | Japanese   |        0.2 |
| United States | Korean     |        0.3 |
| United States | Polish     |        0.3 |
| United States | Portuguese |        0.2 |
| United States | Spanish    |        7.5 |
| United States | Tagalog    |        0.4 |
| United States | Vietnamese |        0.2 |
+---------------+------------+------------+
12 rows in set
```

**13_Pareil aux UK.**
```sql
SELECT country.Name, countrylanguage.language, countrylanguage.Percentage from countrylanguage
JOIN country ON countrylanguage.CountryCode = country.Code
WHERE country.Name LIKE "United Kingdom";

+----------------+----------+------------+
| Name           | language | Percentage |
+----------------+----------+------------+
| United Kingdom | English  |       97.3 |
| United Kingdom | Gaeli    |        0.1 |
| United Kingdom | Kymri    |        0.9 |
+----------------+----------+------------+
```

**14_Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.**

WORK IN PROGRESS

**15_Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi ?**
In some cases is 100, and even more for the Netherlands and Samoa, in general is less than 100, and even 0. Depending on the information available at the time of the creation of the database

```sql
SELECT country.Name, SUM(countrylanguage.Percentage) AS TotalPercentage FROM country
JOIN countrylanguage ON countrylanguage.CountryCode = country.Code
GROUP BY countrylanguage.CountryCode ORDER BY TotalPercentage DESC LIMIT 30;
--Output reduced to 50
+-----------------------+-----------------+
| Name                  | TotalPercentage |
+-----------------------+-----------------+
| Netherlands           |           101.0 |
| Samoa                 |           100.1 |
| Bhutan                |           100.0 |
| Guyana                |           100.0 |
| Bahamas               |           100.0 |
| Costa Rica            |           100.0 |
| Cape Verde            |           100.0 |
| Haiti                 |           100.0 |
| Bermuda               |           100.0 |
| San Marino            |           100.0 |
| El Salvador           |           100.0 |
| Tuvalu                |           100.0 |
| Dominican Republic    |           100.0 |
| Greenland             |           100.0 |
| Western Sahara        |           100.0 |
| Dominica              |           100.0 |
| Grenada               |           100.0 |
| Chile                 |           100.0 |
| Maldives              |           100.0 |
| Algeria               |           100.0 |
| Ireland               |           100.0 |
| Palestine             |           100.0 |
| North Korea           |           100.0 |
| South Korea           |           100.0 |
| Rwanda                |           100.0 |
| Japan                 |           100.0 |
| Saint Kitts and Nevis |           100.0 |
| Cuba                  |           100.0 |
| Poland                |           100.0 |
| Lesotho               |           100.0 |
| Saint Lucia           |           100.0 |
| Faroe Islands         |           100.0 |
| Ecuador               |           100.0 |
| Jordan                |            99.9 |
| Nauru                 |            99.9 |
| Nicaragua             |            99.9 |
| Hungary               |            99.8 |
| Trinidad and Tobago   |            99.8 |
| Albania               |            99.8 |
| Colombia              |            99.7 |
| Cambodia              |            99.6 |
| Turkey                |            99.6 |
| Yemen                 |            99.6 |
| Italy                 |            99.6 |
| Slovakia              |            99.5 |
| Iraq                  |            99.5 |
| Belarus               |            99.5 |
| Sri Lanka             |            99.5 |
| Greece                |            99.4 |
| Romania               |            99.4 |
+-----------------------+-----------------+
50 rows in set 
```

**16_Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur)**

![Alt text](region_map.html)

## PART 2: MODIFICATION DU MODELE
**Faire une version 2 du modèle de donnée. On veut apporter les changements suivants:**
* Utiliser classicmodel.customer.country pour stocker la clef du pays venant de la table world.country
* Idem pour classicmodel.offices.country.
* Remplacer le type du champ classicmodel.orders.status par un enum de tous les états différents dans la base de données.
* Ajouter un champ classicmodel.customer.language qui devra contenir le code de la langue avec laquelle s’adresser au client en ISO 639-1.
* Écrire le code SQL permettant de faire ces modifications dans un fichier migration.sql.

**Migration de la base.**
Pour appliquer notre migration on doit:
* Créer une nouvelle BDD sur le même modèle que la première (sans les données).
* Appliquer la migration (changer le modèle).
* Importer les données depuis la première base de données en appliquant les modifications nécessaires (par exemple remplacer le nom du pays par la PK de world.country).
> pour la langue des clients, on mettra la première (la plus courante) langue officielle du pays.

**The Migration is not complete. The column language in customers is not yet filled with the ISO639-1 code**

### SQL REQUESTs OF PART 2

**1_Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.**
```sql
SELECT c.country,COUNT(DISTINCT c.customerName) AS numberOfClients, language, SUM(p.amount) AS totalRevenue FROM customers c
JOIN payments p ON c.customerNumber = p.customerNumber GROUP BY c.country;

--The output misses the language. Also, only shows 21 countries, as there are some countries (RUS, POL, ISR, etc) 
-- with clients but with no payments done, therefore they are not shown
+---------+-----------------+--------------+
| country | numberOfClients | totalRevenue |
+---------+-----------------+--------------+
| AUS     |               5 |    509385.82 |
| AUT     |               2 |    136119.99 |
| BEL     |               2 |     91471.03 |
| CAN     |               3 |    205911.86 |
| CHE     |               1 |    108777.92 |
| DEU     |               3 |    196470.99 |
| DNK     |               2 |    197356.30 |
| ESP     |               5 |    994438.53 |
| FIN     |               3 |    295149.35 |
| FRA     |              12 |    965750.58 |
| GBR     |               5 |    391503.90 |
| HKG     |               1 |     45480.79 |
| IRL     |               1 |     49898.27 |
| ITA     |               4 |    325254.55 |
| JPN     |               2 |    167909.95 |
| NOR     |               3 |    270846.30 |
| NZL     |               4 |    392486.59 |
| PHL     |               1 |     87468.30 |
| SGP     |               2 |    261671.60 |
| SWE     |               2 |    120457.09 |
| USA     |              35 |   3040029.52 |
+---------+-----------------+--------------+
21 rows in set 
```

**2_La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.**

```sql
SELECT customers.customerName, SUM(orderdetails.quantityOrdered * orderdetails.priceEach) AS TotalRevenue FROM customers
JOIN orders ON customers.customerNumber = orders.customerNumber
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
GROUP BY customers.customerName ORDER BY TotalRevenue DESC LIMIT 10;

+------------------------------+--------------+
| customerName                 | TotalRevenue |
+------------------------------+--------------+
| Euro+ Shopping Channel       |    820689.54 |
| Mini Gifts Distributors Ltd. |    591827.34 |
| Australian Collectors, Co.   |    180585.07 |
| Muscle Machine Inc           |    177913.95 |
| La Rochelle Gifts            |    158573.12 |
| Dragon Souveniers, Ltd.      |    156251.03 |
| Down Under Souveniers, Inc   |    154622.08 |
| Land of Toys Inc.            |    149085.15 |
| AV Stores, Co.               |    148410.09 |
| The Sharp Gifts Warehouse    |    143536.27 |
+------------------------------+--------------+
10 rows in set
```
**3_La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).**
```sql
SELECT AVG(DATEDIFF(shippedDate, orderDate)) AS total_average_days
FROM orders;
+--------------------+
| total_average_days |
+--------------------+
|             3.7564 |
+--------------------+
```
**4_Les 10 produits les plus vendus.**
```sql
SELECT products.productName, SUM(orderdetails.quantityOrdered) AS TotalQuantity FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY TotalQuantity DESC limit 10;

+-----------------------------------------+---------------+
| productName                             | TotalQuantity |
+-----------------------------------------+---------------+
| 1992 Ferrari 360 Spider red             |          1808 |
| 1937 Lincoln Berline                    |          1111 |
| American Airlines: MD-11S               |          1085 |
| 1941 Chevrolet Special Deluxe Cabriolet |          1076 |
| 1930 Buick Marquette Phaeton            |          1074 |
| 1940s Ford truck                        |          1061 |
| 1969 Harley Davidson Ultimate Chopper   |          1057 |
| 1957 Chevy Pickup                       |          1056 |
| 1964 Mercedes Tour Bus                  |          1053 |
| 1956 Porsche 356A Coupe                 |          1052 |
+-----------------------------------------+---------------+
10 rows in set
```

**5_Pour chaque pays le produits le plus vendu.**


**6_Le produit qui a rapporté le plus de bénéfice.**
```sql
SELECT products.productName, SUM(orderdetails.quantityOrdered*orderdetails.priceEach) AS MaxRevenu FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
GROUP BY products.productName ORDER BY MaxRevenu DESC limit 1;

+-----------------------------+-----------+
| productName                 | MaxRevenu |
+-----------------------------+-----------+
| 1992 Ferrari 360 Spider red | 276839.98 |
+-----------------------------+-----------+
```
**7_La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).**
```sql
SELECT products.productName, 
AVG(((products.MSRP - orderdetails.priceEach) / products.MSRP) * 100) AS average_percentage FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode 
GROUP by products.productName ORDER BY products.productName LIMIT 20;

+-------------------------------------+--------------------+
| productName                         | average_percentage |
+-------------------------------------+--------------------+
| 18th century schooner               |       9.7407196296 |
| 18th Century Vintage Horse Carriage |      10.0707327857 |
| 1900s Vintage Bi-Plane              |      10.0032320714 |
| 1900s Vintage Tri-Plane             |       6.5355418571 |
| 1903 Ford Model A                   |       7.9624612963 |
| 1904 Buick Runabout                 |      10.7401922593 |
| 1911 Ford Town Car                  |      10.3191278400 |
| 1912 Ford Model T Delivery Wagon    |      11.4458711111 |
| 1913 Ford Model T Speedster         |       7.7848048214 |
| 1917 Grand Touring Sedan            |       9.6000000000 |
| 1917 Maxwell Touring Car            |      11.0004751429 |
| 1926 Ford Fire Engine               |       7.6423987857 |
| 1928 British Royal Navy Airplane    |      10.3565552500 |
| 1928 Ford Phaeton Deluxe            |       9.6068780714 |
| 1928 Mercedes-Benz SSK              |      11.1777777500 |
| 1930 Buick Marquette Phaeton        |      11.1112019643 |
| 1932 Alfa Romeo 8C2300 Spider Sport |       9.8380960800 |
| 1932 Model A Ford J-Coupe           |      10.1423177857 |
| 1934 Ford V8 Coupe                  |      10.7503087143 |
| 1936 Chrysler Airflow               |       7.8579495714 |
+-------------------------------------+--------------------+
20 rows in set 
```

**8_Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.**

