USE `world`;

DROP USER IF EXISTS lolam1@localhost;
CREATE USER lolam1@localhost IDENTIFIED BY 'lolam1';
GRANT ALL PRIVILEGES ON `world`.* TO 'lolam1'@'localhost';