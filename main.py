import plotly.express as px
import pandas as pd
import mysql.connector

# https://plotly.com/python/choropleth-maps/
# Question 15 Map of the Regions in the world, from world DB:

mydb = mysql.connector.connect(
    host="localhost",
    user="lolam1",
    password="lolam1",
    database="world"
)

cursor = mydb.cursor(dictionary=True)
query = "SELECT Name, Region, Code FROM country;"
result_df = pd.read_sql(query,mydb)


fig = px.choropleth(result_df, locations="Code",
                    color="Region",
                    hover_name= "Name", # column to add to hover information
                    # hover_data= "Code",
                    color_continuous_scale=px.colors.sequential.Plasma)
fig.write_html("./region_map.html")
#fig.show()

## PART 2: Modification BBDD classicmodels to add column language (in format ISO639-1) to customers table. 
# Read the csv containing the ISO639-1 2 digits codes and the associated language 

data = []

with open('language-codes_csv.csv') as csv:
    for line in csv.readlines():
        inputs = line.split(",")  
        language_code = inputs[0].strip()
        
        # Extract language from the second field and remove spaces
        language_gross = inputs[1].split(';')
        language = language_gross[0].replace('"', '').strip()
        
        data.append([language_code, language])

# Create a DataFrame from the list
columns = ['language_code', 'language']
csv_df = pd.DataFrame(data, columns=columns)

#print(csv_df)

# loop through the rows using iterrows()
cursor1= mydb.cursor()
countryCode = []
for index, row in csv_df.iterrows():
    language_code = row['language_code']
    language = row['language']
    #print(language_code, language)
    cursor1.execute("""
        SELECT cl.countryCode, cl.language, cl.Percentage FROM countrylanguage AS cl
        JOIN (SELECT countryCode,MAX(Percentage) AS maxPercentage FROM countrylanguage GROUP BY countryCode) max_lang
        ON cl.countryCode = max_lang.countryCode AND cl.Percentage = max_lang.maxPercentage WHERE cl.language LIKE %s;""", [language])

    result = cursor1.fetchall()
    print(result)
### THERE ARE MANY COUNTRIES WITH THE SAME LANGAGE, THE LOOP SHOULD GO THE OTHER WAY, LOOPING THROUGH THE SQL
    # AND COMPARING IT TO THE csv_df!!!!!!!!!!!!!!!!!!



# # Put the SQL query (getting the countryCode and language) in another DF
    
# cursor1= mydb.cursor()

# cursor1.execute("""
#     SELECT cl.countryCode, cl.language, cl.Percentage FROM countrylanguage AS cl
#     JOIN (SELECT countryCode,MAX(Percentage) AS maxPercentage FROM countrylanguage GROUP BY countryCode) max_lang
#     ON cl.countryCode = max_lang.countryCode AND cl.Percentage = max_lang.maxPercentage;
# """)
# result = cursor1.fetchall()

# # Create a DataFrame from the SQL query result
# sql_query_df = pd.DataFrame(result, columns=['CountryCode', 'Language', 'Percentage'])
# print(sql_query_df)
    
mydb.close()
